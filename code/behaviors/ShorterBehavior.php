<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 * Date: 19.10.18
 * Time: 17:26
 */

namespace app\behaviors;


use app\models\Links;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class ShorterBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
        ];
    }

    public function beforeInsert($event)
    {
        /**
         * @var Links $model
         */
        $model = $event->sender;
        $model->shortcat = \Yii::$app->security->generateRandomString(10);
    }
}
