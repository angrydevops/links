<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Links;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class ClearController extends Controller
{
    /**
     * Очитка ссылок по времени
     *
     * @return int Exit code
     */
    public function actionIndex()
    {
        /**
         * @var array $model
         */
        $model = Links::find()->where(['>', 'duration', new Expression("NOW()")])
            ->andWhere(['status' => 1])->asArray()
            ->all();
        $message = null;

        if (empty($model)) {
            $message = "Записей нет \n";
            $this->stdout($message, Console::FG_RED);
            Yii::info($message, 'console');
            return ExitCode::OK;
        }
        $ids = ArrayHelper::getColumn($model, 'id');

        Links::updateAll(['status' => 0], ['IN', 'id', $ids]);

        $message = "Статусы обновлены, количество записей " . count($ids) . "\n";
        $this->stdout($message, Console::FG_GREEN);
        Yii::info($message, 'console');
        return ExitCode::OK;
    }
}
