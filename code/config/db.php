<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mariadb;dbname=dev',
    'username' => 'dev',
    'password' => 'dev',
    'charset' => 'utf8',
];
