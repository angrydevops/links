<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = 'mysql:host=mariadb;dbname=test';
$db['username'] = 'dev';
$db['password'] = 'dev';

return $db;
