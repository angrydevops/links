<?php

namespace app\controllers;

use app\models\Links;
use app\models\search\LinksSearch;
use app\models\search\StatisticsSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class LinksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LinksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param integer $id
     *
     * @return Links the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Links::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Links();
        if ($model->load(Yii::$app->request->post())) {
            if (!$model->save()) {
                Yii::error("Ошибка добаление записи " . VarDumper::dumpAsString($model->firstErrors), 'frontend');
            }
            Yii::info("Ссылка с id  {$model->id} успешно создана\n", 'frontend');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->save()) {
                Yii::error("Ошибка обновление записи " . VarDumper::dumpAsString($model->firstErrors), 'frontend');
            }
            Yii::info("Ссылка с id  {$model->id} успешно обновлена\n", 'frontend');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = 0;
        if(!$model->update()){
            Yii::error("Ошибка обновление записи " . VarDumper::dumpAsString($model->firstErrors), 'frontend');
        }
        Yii::info("Ссылка с id  {$model->id} успешно удалена\n", 'frontend');
        return $this->redirect(['index']);
    }

    /**
     * @return string
     */
    public function actionInfo()
    {
        $searchModel = new StatisticsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('info', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
