<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\Links;
use app\models\LoginForm;
use app\models\Statistics;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader;
use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * @param $shortcat
     *
     * @throws Reader\InvalidDatabaseException
     */
    public function actionRedirect($shortcat)
    {
        $model = Links::find()->where(['shortcat' => $shortcat, 'status' => 1])->one();
        /**
         * @var Browser $browser
         */
        $browser = new Browser();
        /**
         * @var Os $os
         */
        $os = new Os();

        $ip = Yii::$app->request->getUserIP();

        $reader = new \GeoIp2\Database\Reader(Yii::getAlias('@app/GeoLite2-City.mmdb'));
        try {
            $record = $reader->city($ip);

        } catch (AddressNotFoundException $e) {
            $record = [];
        } catch (Reader\InvalidDatabaseException $e) {
        }

        $statistics = new Statistics();
        $statistics->link_id = $model->id;
        $statistics->browser = $browser->getName();
        $statistics->ip = $ip;
        $statistics->geo = Json::encode([
            'country' => $record->country->names['ru'] ?? null,
            'city' => $record->city->names['ru'] ?? null,
        ]);

        $statistics->user_agent = Json::encode([
                    'os' => $os->getName(),
                    'is_mobile' =>$os->getIsMobile() ?? false,
                    'browser' => [
                       'version' => $browser->getVersion(),
                       'is_robot' => $browser->getIsRobot() ?? false
                    ]
                ])
        ;

        $statistics->save();


        $this->redirect($model->destination);
    }
}
