<?php
use yii\db\Migration;
use app\models\User;

class m000000_000000_create_user_table extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $user = new User();
        $user->username = 'admin';
        $user->auth_key = 'admin';
        $user->password_hash = 'admin';
        $user->password_reset_token = 'admin';
        $user->email = 'admin@admin.ru';
        $user->save();
    }

    public function down()
    {
        $this->dropTable('user');
    }

}
