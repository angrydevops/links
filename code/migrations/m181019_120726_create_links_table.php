<?php

use yii\db\Migration;

/**
 * Handles the creation of table `links`.
 */
class m181019_120726_create_links_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%links}}', [
            'id' => $this->primaryKey(),
            'destination' => $this->string(512)->comment('Целевая ссылка'),
            'shortcat' => $this->string(10)->comment('Сокращенная ссылка'),
            'duration' => $this->timestamp()->null()->comment('Время жизни ссылки'),
            'status' => $this->integer(1)->defaultValue(1)->comment('Статус'),
            'created_by' => $this->integer()->notNull()->defaultValue(1)->comment('id - пользователя(users -> id)'),
            'created_at' => $this->timestamp()->comment('время создания'),
        ], $tableOptions);

        $this->createIndex(
            'idx-links-created_by',
            '{{%links}}',
            'created_by'
        );

        $this->addForeignKey(
            'fk-links-created_by',
            '{{%links}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addCommentOnTable('{{%links}}', 'Таблица сылок');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-links-created_by',
            '{{%links}}'
        );


        $this->dropTable('links');
    }
}
