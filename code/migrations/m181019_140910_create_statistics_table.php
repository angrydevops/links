<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statistics`.
 */
class m181019_140910_create_statistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%statistics}}', [
            'id' => $this->primaryKey(),
            'link_id' => $this->integer()->comment('Ссылка'),
            'date' => $this->timestamp()->notNull()->comment('Время визита'),
            'geo' => $this->json()->comment('Гео данные'),
            'ip' => $this->string()->comment('IP'),
            'browser' => $this->string()->comment('Браузер'),
            'user_agent' => $this->json()->comment('User-Agent')
        ], $tableOptions);

        $this->addForeignKey(
            'fk-statistics-link_id',
            '{{%statistics}}',
            'link_id',
            '{{%links}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-statistics-link_id',
            '{{%statistics}}'
        );

        $this->dropTable('statistics');
    }
}
