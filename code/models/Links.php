<?php

namespace app\models;

use app\behaviors\ShorterBehavior;
use app\models\query\LinksQuery;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%links}}".
 *
 * @property int        $id
 * @property string     $destination  Целевая ссылка
 * @property string     $shortcat     Сокращенная ссылка
 * @property string     $duration     Время жизни ссылки
 * @property int        $status       Статус
 * @property int        $created_by   id - пользователя(users -> id)
 * @property string     $created_at   время создания
 *
 * @property User       $createdBy
 * @property Statistics $statistics
 * @property Statistics $infoStatistics
 */
class Links extends \yii\db\ActiveRecord
{
    public static $counts;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%links}}';
    }

    /**
     * {@inheritdoc}
     * @return LinksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LinksQuery(get_called_class());
    }

    public function behaviors()
    {
        return
            [
                [
                    'class' => AttributeBehavior::class,
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ],
                    'value' => new Expression('NOW()'),
                ],
                [
                    'class' => ShorterBehavior::class,
                ]
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duration', 'created_at'], 'safe'],
            [['status', 'created_by'], 'integer'],
            [['destination'], 'url'],
            [['destination'], 'string', 'max' => 512],
            [['shortcat'], 'string', 'max' => 10],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['created_by' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'destination' => Yii::t('app', 'Целевая ссылка'),
            'shortcat' => Yii::t('app', 'Сокращенная ссылка'),
            'duration' => Yii::t('app', 'Время жизни ссылки'),
            'status' => Yii::t('app', 'Статус'),
            'created_by' => Yii::t('app', 'id - пользователя(users -> id)'),
            'created_at' => Yii::t('app', 'время создания'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistics()
    {
        return $this->hasMany(Statistics::class, ['link_id' => 'id']);
    }

    /**
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function getInfoStatistics()
    {
        return Statistics::find()->select([
            'date',
            'ip',
            'browser',
            'geo',
            'user_agent',
            'COUNT(`ip`) as counts',
        ])->where(['link_id' => $this->id])->groupBy(['ip', 'browser'])->createCommand()->queryAll();
    }
}
