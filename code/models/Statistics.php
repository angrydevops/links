<?php

namespace app\models;

use app\models\query\StatisticsQuery;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%statistics}}".
 *
 * @property int    $id
 * @property int    $link_id    Ссылка
 * @property string $date       Время визита
 * @property string $geo        Гео данные
 * @property string $ip         IP
 * @property string $browser    Браузер
 * @property string $user_agent User-Agent
 *
 * @property Links  $link
 */
class Statistics extends \yii\db\ActiveRecord
{
	/**
	 * @var integer
	 */
    public $counts;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistics}}';
    }

    /**
     * {@inheritdoc}
     * @return StatisticsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatisticsQuery(get_called_class());
    }

    public function behaviors()
    {
        return
            [

                [
                    'class' => AttributeBehavior::class,
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => 'date',
                    ],
                    'value' => new Expression('NOW()')
                    ,
                ],
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id'], 'integer'],
            [['date', 'counts'], 'safe'],
            [['geo', 'user_agent'], 'string'],
            [['ip', 'browser'], 'string', 'max' => 255],
            [
                ['link_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Links::class,
                'targetAttribute' => ['link_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link_id' => Yii::t('app', 'Ссылка'),
            'date' => Yii::t('app', 'Время последнего визита'),
            'geo' => Yii::t('app', 'Гео данные'),
            'ip' => Yii::t('app', 'IP'),
            'browser' => Yii::t('app', 'Браузер'),
            'user_agent' => Yii::t('app', 'User-Agent'),
            'counts' => Yii::t('app', 'Кол-во переходов'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(Links::class, ['id' => 'link_id']);
    }
}
