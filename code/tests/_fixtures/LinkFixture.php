<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 */
namespace tests\_fixtures;

use yii\test\ActiveFixture;

class LinkFixture extends ActiveFixture
{
	public $modelClass = 'app\models\Links';
	public $dataFile = '@tests/_fixtures/data/link.php';
}
