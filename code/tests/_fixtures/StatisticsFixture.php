<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 */
namespace tests\_fixtures;

use yii\test\ActiveFixture;

class StatisticsFixture extends ActiveFixture
{
	public $modelClass = 'app\models\Statistics';
	public $dataFile = '@tests/_fixtures/data/statistics.php';
}
