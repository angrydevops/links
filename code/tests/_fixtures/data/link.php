<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 */
return [
	[
		'id' => 1,
		'destination' => 'http://site1.com',
		'shortcat' => 'HeUwy3H7XR',
		'duration' => null,
		'status' => 1,
		'created_by' => 1,
		'created_at' => '2018-11-01 02:49:23',
	],
	[
		'id' => 2,
		'destination' => 'http://site2.com',
		'shortcat' => 'y3HHeUw7XR',
		'duration' => null,
		'status' => 1,
		'created_by' => 1,
		'created_at' => '2018-11-01 02:49:23',
	],

];
