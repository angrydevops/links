<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 * Date: 31.10.18
 * Time: 22:24
 */
namespace tests\models;

use app\models\Links;
use app\models\User;
use Codeception\Test\Unit;
use tests\_fixtures\LinkFixture;
use Codeception\Specify;

class LinksTest extends Unit
{
    use Specify;

    /**
     * @var \UnitTester
     */
    public $tester;
    /**
     * @var Links
     */
    private $model;

	public function _before()
	{
		$this->tester->haveFixtures([
			'link' => [
				'class' => LinkFixture::class,
				'dataFile' => '@tests/_fixtures/data/link.php'
			]
		]);
	}

    public function testSaveRecord()
    {

		$fixture = $this->tester->grabFixture('link', 0);

        $this->model =  Links::findOne($fixture['id']);

        $this->specify("Запись в бд", function () {
            $this->model->destination = 'https://github.com/';
            $this->tester->assertTrue( $this->model->save(), 'Попытка сохранения');
        });


        $this->specify("Проверка наличая записи", function () {
            $this->tester->seeRecord('app\models\Links', ['destination' => 'https://github.com/', 'status' => 1], 'Запись найдена');
            $this->tester->dontSeeRecord('app\models\Links', ['destination' => 'https://vk.com/', 'status' => 1]);
        });
    }


    public function testValidation()
    {
        $this->model = new Links();


        $this->specify("shortcat большой", function () {
            $this->model->shortcat = "asdadadasdsaadasdasd";
            $this->tester->assertFalse($this->model->validate(['shortcat']), 'shortcat не валиден');
        });

        $this->specify("shortcat стандартный", function () {
            $this->model->shortcat = "0123456789";
            $this->tester->assertTrue($this->model->validate(['shortcat']), 'shortcat валиден');
        });

        $this->specify("status не число", function () {
            $this->model->status = "string";
            $this->tester->assertFalse($this->model->validate(['status']), 'status не валиден');
        });

        $this->specify("status число", function () {
            $this->model->status = 23;
            $this->tester->assertTrue($this->model->validate(['status']), 'status валиден');
        });

        $this->specify("destination не url", function () {
            $this->model->destination = "nourl";
            $this->tester->assertFalse($this->model->validate(['destination']), 'destination не валиден');
        });

        $this->specify("destination  url", function () {
            $this->model->destination = "https://site.com";
            $this->tester->assertTrue($this->model->validate(['destination']), 'destination  валиден');
        });

        $this->specify("проверка связи к users", function () {
            /**
             * @var User $user
             */
            $user = User::findOne([1]);
            $this->model->created_by = $user->id;

            $this->tester->assertInstanceOf(User::class, $this->model->createdBy, 'связь user валидна');
        });
    }

}
