<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 * Date: 31.10.18
 * Time: 22:24
 */

namespace tests\models;

use app\models\Links;
use app\models\Statistics;
use Codeception\Specify;
use Codeception\Test\Unit;
use tests\_fixtures\LinkFixture;
use tests\_fixtures\StatisticsFixture;
use yii\db\Expression;
use yii\helpers\Json;

class StatsitcsTest extends Unit
{
	use Specify;

	/**
	 * @var \UnitTester
	 */
	public $tester;
	/**
	 * @var Statistics
	 */
	private $model;

	public function _before()
	{
		$this->tester->haveFixtures([
			'link' => [
				'class' => LinkFixture::class,
				'dataFile' => '@tests/_fixtures/data/link.php'
			],
			'statistics' => [
				'class' => StatisticsFixture::class,
				'dataFile' => '@tests/_fixtures/data/statistics.php',
			],
		]);
	}

	public function testSaveRecord()
	{

		$fixture = $this->tester->grabFixture('statistics', 1);

		$this->model = Statistics::findOne($fixture['id']);

		$this->specify("Запись в бд", function () {
			$this->model->link_id = 2;
			$this->model->geo = Json::encode([
				'country' => 'Russia',
				'city' => 'Moscow',
			]);
			$this->model->user_agent = Json::encode([
				'os' => 'windows',
				'is_mobile' => true,
				'browser' => [
					'version' => '3',
					'is_robot' => false,
				],
			]);
			$this->tester->assertTrue($this->model->save(), 'Попытка сохранения ');

		});


		$this->specify("Проверка наличая записи", function () {
			$this->tester->seeRecord('app\models\Statistics', ['id' => $this->model->id, 'link_id' => 2]);
			$this->tester->dontSeeRecord('app\models\Statistics', ['id' => 21, 'link_id' => 2]);
		});

	}


    public function testValidation()
    {
        $this->model = new Statistics();


		$this->specify("link_id ссылка на Links", function () {
			$fixture = $this->tester->grabFixture('link', 1);
			$link =  Links::findOne($fixture['id']);
			$this->model->link_id = $link->id;
			$this->tester->assertInstanceOf(Links::class, $this->model->link);
		});



        $this->specify("date коректно сгенерирован", function () {
			$fixture = $this->tester->grabFixture('statistics', 1);
			$this->model->date = $fixture['date'];
			$this->tester->assertRegExp(
				'/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\z/',
				$this->model->date, 'date коректна');
        });


        $this->specify("ip коректно получен", function () {
			$fixture = $this->tester->grabFixture('statistics', 2);
			$this->model->ip = $fixture['ip'];
			$this->tester->assertRegExp(
				'/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/',
				$this->model->ip, 'ip коректен');
        });
    }
}
