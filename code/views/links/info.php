<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\StatisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Statistics info');
$this->params['breadcrumbs'][] = ['label' =>'Инфо' ,'url' => Url::previous()];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ip',
            'browser',
            'user_agent',
            'geo',

        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
