<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Links */
Url::remember();
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$link = Url::base(true) . '/' . ($model->shortcat);
?>
<div class="links-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'destination',
            [
                'attribute' => 'shortcat',
                'value' => Html::a($link, $link, []),
                'format' => 'html',
            ],
            'duration',
            'status',
        ],
    ]);
    ?>
    <h2>Статистика переходов</h2>
    <?php

    foreach ($model->infoStatistics as $statistics) {
        $model = new \app\models\Statistics();
        $model->load($statistics, '');

        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'date',
                    'value' => $model->date . ' ' . Html::a('Подробная статистика переходов',
                            Url::toRoute(['links/info', 'browser' => $model->browser, 'ip' => $model->ip])),
                    'format' => 'html',
                ],
                'ip',
                'browser',
                'geo',
                'user_agent',
                'counts',
            ],
        ]);
    }
    ?>

</div>
